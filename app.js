var express = require('express');
var app = express();
var session = require('express-session');
var userRoute = require('./routes/userRoute');
var authRoute = require('./routes/authRoute');
var homeRoute = require('./routes/homeRoute');
var bodyparser = require('body-parser');

app.set('trust proxy', 1);
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));

app.use('/user', userRoute);
app.use(authRoute);
app.use('/home', homeRoute);


app.listen(7000);

module.exports = app;
 


