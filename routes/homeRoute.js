var express = require('express');
var routes = express.Router();
var jwt = require('jsonwebtoken');


routes.get('', function(req, res){
    let token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, 'secret', function(err) {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            return res.send({ error : false, message: 'welcome home'});    
    });
});

module.exports = routes;