var express = require('express');
var routes = express.Router();
var connection = require('../lib/connection');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

routes.get('', function(req, res){
    // let token = req.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    // jwt.verify(token, 'secret', function(err) {
    //     if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        connection.query('SELECT * FROM users', function(error, result, fields){
            if(error) throw error;
                let data = result;
                return res.send({ error : false, data: data, message: 'users list.'})
        });
    // });
});

routes.post('/add', function(req, res){
    let data = req.body;
    let cryptoPassword = crypto.createHash('md5').update(data.password).digest("hex");
    connection.query("INSERT INTO users SET ? ", {
            email: data.email, 
            fullname: data.fullname, 
            password: cryptoPassword }, function (error, results) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New user has been created successfully.'});
    });
});

routes.delete('/delete', function(req, res){
    let token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    jwt.verify(token, 'secret', function(err) {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        let username = req.body.username;
        connection.query('DELETE FROM users WHERE username = ?', [username], function(error, results, fields){
            if (error) throw error;
            return res.send({ error: false, data: results, message: 'User has been Deleted successfully.'});
        });
    });
});

routes.put('/updatepassword', function(req,res){
    // let token = req.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    // jwt.verify(token, 'secret', function(err) {
    //     if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
            let data = req.body;
            let newPassword =  crypto.createHash('md5').update(data.newpassword).digest("hex");
            data.password = crypto.createHash('md5').update(data.password).digest("hex");
            connection.query("SELECT * FROM users WHERE email = ? AND password = ?", [data.email, data.password], function(error, result){
                if(error) throw error;
                    if(result != ''){
                        connection.query("UPDATE users SET password = ? WHERE email = ?", [newPassword, data.email], function(error, result){
                            if(error) throw error;
                        });
                    }
                    return res.send({ error: false, message: 'Password berhasil dirubah'});
            });
    // });
})

module.exports = routes;