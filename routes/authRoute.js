var express = require('express');
var routes = express.Router();
var connection = require('../lib/connection');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');


routes.post('/login' , function(req, res){
    let email = req.body.email;
    let password = crypto.createHash('md5').update(req.body.password).digest("hex");
    connection.query('SELECT * FROM users WHERE email = ? AND password = ?', [email, password] , function(error, result){
        if(error) throw error;
            if(result != ''){
                let data = result;
                let JWTToken = jwt.sign({
                    email: result.email,
                    id: result.id
                },
                'secret',
                {
                    expiresIn: 86400
                });
        
                return res.status(200).send({
                    auth: true,
                    token: JWTToken
                });
            }else{
                return res.send({ error : false, message: 'Auth Failed!! Data tidak cocok'});
            }
    });
});

routes.get('/logout' , function(req, res){
    return res.status(200).send({ auth: false, token: null });
});

routes.post('/forgotpassword' , function(req, res){
    let email = req.body.email;
    connection.query('SELECT * FROM users WHERE email = ? ', [email] , function(error, result){
        if(error) throw error;
            if(result != ''){
                let data = result;
                let newPassword = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                let cryptnewPassword = crypto.createHash('md5').update(newPassword).digest("hex");
                console.log(newPassword);
                connection.query("UPDATE users SET password = ? WHERE email = ?", [cryptnewPassword, data[0].email], function(error, result){
                    if(error) throw error;
                });
                return res.send({ error : false, data: newPassword, message: 'Password berhasil diubah'});
            }else{
                return res.send({ error : false, message: 'Data tidak ditemukan'});
            }
    });
});


module.exports = routes;
